(function($) {})(window.jQuery);

jQuery(document).ready(function() {

    function scrollJump(el) {
        $('html, body').animate({
            scrollTop: $(el).offset().top
        }, 750);
    };
    // Scroll to on click
    $(".btn[data-jump]").on("click", function(event){
		event.preventDefault();
		scrollJump($(this).data("jump"));
	});
    
    //FORM VALIDATION
    // On submit, check if all inputs are valid
    $("button[type=submit]").click(function(e){
        event.preventDefault();
		if( $("form").get(0).checkValidity() )
		{
            // Shows ok message
            $(".form--message").fadeOut(500);
            $(".form--message.form--message-ok").fadeIn(500);
		}
		else
		{   
            // Check if all inputs are valid
            $( "input, select, textarea" ).each(function() {
                if( $(this).get(0).checkValidity() )
                {
                    $(this).removeClass("invalid");
                }else{
                    $(this).addClass("invalid");
                };
            });
            // Shows error message
			$(".form--message").fadeOut(500);
            $(".form--message.form--message-error").fadeIn(500);
		}
    });

    $("input, select, textarea").blur(function(){
        // Check if each input are valid
        if( $(this).get(0).checkValidity() )
		{
            $(this).removeClass("invalid");
		}else{
            $(this).addClass("invalid");
        }
    });

    $("input, select, textarea").focusout(function(){
        // Check if all inputs are valid
        if( $("form").get(0).checkValidity() )
		{
            $(".form--message").fadeOut(500);
		}
    });
    

});
