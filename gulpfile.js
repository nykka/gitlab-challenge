var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var cssnano = require('gulp-cssnano');
var autoprefixer = require('gulp-autoprefixer');

// styles task
gulp.task('styles', function(){
    gulp.src('./src/sass/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./dist/css/'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cssnano())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./dist/css/'))
});

//watch task
gulp.task('watch', function(){
    gulp.watch('./src/sass/*.scss', ['styles']); 
})

//define default task
gulp.task('default', ['styles', 'watch']);